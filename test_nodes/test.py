#!/usr/bin/env python
from hotrider_lib.hotrider import Hotrider
import rospy
import math

robot = Hotrider('hotrider')
"""
print("*******")
print(robot.center)
print(robot.front)
print(robot.left)
print(robot.back)
print(robot.right)
print("*******")
"""
way_out = True
while way_out:
    way_out = False
    if robot.front == robot.center + 1:
        robot.move_forward()
        way_out = True
    elif robot.right == robot.center + 1:
        robot.turn_right()
        way_out = True
    elif robot.left == robot.center + 1:
        robot.turn_left()
        way_out = True
